import {Component} from '@angular/core';
import {FieldType} from '@ngx-formly/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'formly-field-input',
  template: '<input type="input" [formControl]="formControl" [formlyAttributes]="field">'
  ,
})
// tslint:disable-next-line:component-class-suffix
export class FormlyFieldInput extends FieldType {
}
