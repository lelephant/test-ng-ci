import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Todolist} from '../models/todolist';
import {Item} from '../models/item';

@Injectable({
  providedIn: 'root'
})
export class TodolistService {

  constructor(private httpClient: HttpClient) {
  }

  createTodoList(idUser: number): Observable<Todolist> {
    return this.httpClient.get<Todolist>('http://localhost:8080/todo/create', {headers: {userId: idUser.toString()}});
  }

  createItem(data: any): Observable<Item> {
    return this.httpClient.post<Item>('http://localhost:8080/todo/createItem', data);
  }

  removeItem(id: number, todoId: number): Observable<string> {
    return this.httpClient.delete<string>('http://localhost:8080/todo/deleteItem/' + id + '/' + todoId);
  }

  getTodo(id: number): Observable<Todolist> {
    return this.httpClient.get<Todolist>('http://localhost:8080/todo/todolist/' + id);
  }


}
