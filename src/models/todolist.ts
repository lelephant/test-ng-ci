import {Item} from './item';
import {User} from './user';

export interface Todolist {
  id: number;
  items: Item[];
  user: User;
}
